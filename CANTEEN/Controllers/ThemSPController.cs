﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CANTEEN.Models;
namespace CANTEEN.Controllers
{
    public class ThemSPController : Controller
    {
        QL_CanteenEntities9 DL_SP = new QL_CanteenEntities9();
        // GET: ThemSP
        public ActionResult ThemSP()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Dangnhap", "Home");
            }
            

            return View();
        }

        [HttpPost]

        public ActionResult ThemSP(SANPHAM sANPHAM)
        {
            if(DL_SP.SANPHAMs.Any(y=>y.MASP == sANPHAM.MASP))
            {
                ViewBag.Sanpham = "Sản phẩm đã tồn tại";
                return View();
            }
            else
            {
                DL_SP.SANPHAMs.Add(sANPHAM);
                DL_SP.SaveChanges();
                Session["masp"] = sANPHAM.MASP.ToString();
                Session["tensp"] = sANPHAM.TENSP.ToString();
                Session["giasp"] = sANPHAM.GIA.ToString();
                return RedirectToAction("Danhsachmon");
            }

            

        }

        public ActionResult Danhsachmon(SANPHAM sANPHAM)
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Dangnhap", "Home");
            }
            
            return View(DL_SP.SANPHAMs.ToList());

        }
    }
}