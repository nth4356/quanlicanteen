﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CANTEEN.Models;


namespace CANTEEN.Controllers
{
    public class HomeController : Controller
    {
        QL_CanteenEntities9 DB_QL = new QL_CanteenEntities9();

        // GET: Home
      


        public ActionResult Chu(NHANVIEN nHANVIEN)
        {
            if(Session["Username"] == null)
            {
                return RedirectToAction("Dangnhap","Home");
            }
            return View(DB_QL.NHANVIENs.ToList());
            
        }

        public ActionResult Dangxuat()
        {

            Session.Abandon();
            Session.Clear();

            Session.RemoveAll();
            
            FormsAuthentication.SignOut();
            
            return RedirectToAction("Dangnhap");
        }
        [HttpGet]
        
        public ActionResult Dangnhap()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Dangnhap(NHANVIEN nHANVIEN, CHU cHU)
        {
            var checkDangnhapNV = DB_QL.NHANVIENs.Where(x => x.TENTK.Equals(nHANVIEN.TENTK) && x.MATKHAU.Equals(nHANVIEN.MATKHAU)).FirstOrDefault();
            var checkCHU = DB_QL.CHUs.Where(x => x.TENTK.Equals(cHU.TENTK) && x.MATKHAU.Equals(cHU.MATKHAU)).FirstOrDefault();
            if (checkDangnhapNV != null)
            {
                Session["Username"] = nHANVIEN.TENTK.ToString();
                Session["Password"] = nHANVIEN.MATKHAU.ToString();
                return RedirectToAction("NhanVien","NhanVien");


            }
            else if (checkCHU!=null)

            {
                Session["Username"] = cHU.TENTK.ToString();
                Session["Password"] = cHU.MATKHAU.ToString();
                return RedirectToAction("Chu", "Home");
            }
            else
            {
                ViewBag.Notification = "Sai tài khoản hoặc mật khẩu";
                
            }
            return View();
        }

        public ActionResult DanhsachNV(NHANVIEN nHANVIEN)
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Dangnhap", "Home");
            }
            
            return View(DB_QL.NHANVIENs.ToList());
        }

        public ActionResult Dangki()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Dangnhap", "Home");
            }
            
            return View();
        }

        [HttpPost]

        public ActionResult Dangki(NHANVIEN nHANVIEN)
        {
            if (DB_QL.NHANVIENs.Any(x => x.TENTK == nHANVIEN.TENTK))
            {
                ViewBag.Notification = "Tài khoản đã tồn tại";
                return View();

            }
            else
            {
                DB_QL.NHANVIENs.Add(nHANVIEN);
                DB_QL.SaveChanges();
                Session["Username"] = nHANVIEN.TENTK.ToString();
                Session["Password"] = nHANVIEN.MATKHAU.ToString();
                TempData["Dangkithanhcong"] = "Đã đăng kí thành công ✔";
                return RedirectToAction("DanhsachNV");
            }
        }

        public ActionResult Xoa(string id)
        {
            //var ct = DB_QL.CT_XN.Find(id);
            //DB_QL.CT_XN.Remove(ct);
            //DB_QL.SaveChanges();
            var nv = DB_QL.NHANVIENs.Find(id);
            DB_QL.NHANVIENs.Remove(nv);
            DB_QL.SaveChanges();
            return RedirectToAction("DanhsachNV");
        }



        public ActionResult Sua(string id)
        {
            var nv = DB_QL.NHANVIENs.Find(id);
            return View(nv);
        }
        [HttpPost]
        
        public ActionResult Sua(NHANVIEN nv)
        {
            var cn = DB_QL.NHANVIENs.Find(nv.MANV);

            cn.MANV = nv.MANV;
            cn.TENTK = nv.TENTK;
            cn.MATKHAU = nv.MATKHAU;
            cn.TENNV = nv.TENNV;
            cn.DIACHI = nv.DIACHI;
            cn.SDT = nv.SDT;

            DB_QL.SaveChanges();
            return RedirectToAction("DanhsachNV","Home");
        }
    }
}